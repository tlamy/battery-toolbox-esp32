//
// Created by Thomas Lamy on 12.02.23.
//

#include "bms.h"
#include "smbus.h"

float bat_temp_c() {
    uint16_t data;
    if(smbus_read_word(0x08, &data) < 0) {
        perror("bat_temp_c (0x08)");
    };
    return data / 10.0 - 273.1;
}

float bat_pack_volts() {
    uint16_t data;
    if(smbus_read_word(0x09, &data) < 0) {
        perror("bat_pack_volts (0x09)");
    };
    return data / 1000.0;
}

float bat_cell_volts(unsigned char cell) {
    uint16_t data;
    if (cell < 1 || cell > 4) return 0.0;
    if(smbus_read_word(0x3b + cell, &data) < 0) {
        perror("bat_cell_volts (0x3b)");
    };
    return data / 1000.0;
}

float bat_pack_amps() {
    uint16_t data;
    if(smbus_read_word(0x0a, &data) < 0) {
        perror("bat_pack_amps (0x0a)");
    };
    return data / 1000.0;
}

float bat_pack_amps_average() {
    uint16_t data;
    if(smbus_read_word(0x0b, &data) < 0) {
        perror("bat_pack_amps_average (0x0b)");
    };
    return data / 1000.0;
}

float bat_capacity_remaining() {
    uint16_t data;
    if(smbus_read_word(0x0f, &data) < 0) {
        perror("bat_capacity_remaining (0x0f)");
    };
    return data / 1000.0;
}

float bat_capacity() {
    uint16_t data;
    if(smbus_read_word(0x10, &data) < 0) {
        perror("bat_capacity (0x10)");
    };
    return data / 1000.0;
}

float bat_design_capacity() {
    uint16_t data;
    if(smbus_read_word(0x18, &data) < 0) {
        perror("bat_design_capacity (0x18)");
    };
    return data / 1000.0;
}

float bat_design_volts() {
    uint16_t data;
    if(smbus_read_word(0x19, &data) < 0) {
        perror("bat_design_volts (0x19)");
    };
    return data / 1000.0;
}


uint16_t bat_status() {
    uint16_t data;
    if(smbus_read_word(0x16, &data) < 0) {
        perror("bat_status (0x16)");
    };
    return data;
}

uint16_t bat_cycle_count() {
    uint16_t data;
    if(smbus_read_word(0x17, &data) < 0) {
        perror("bat_cycle_count (0x17)");
    };
    return data;
}

unsigned char *bat_manufacturer(unsigned char *buffer) {
    return smbus_read_cstring(0x20, buffer, 32);
}

unsigned char *bat_devicename(unsigned char *buffer) {
    return smbus_read_cstring(0x21, buffer, 32);
}

unsigned char *bat_chemistry(unsigned char *buffer) {
    return smbus_read_cstring(0x22, buffer, 32);
}

void bat_manufactured_date(struct date *date) {
    uint16_t packed_date;
    smbus_read_word(0x1b, &packed_date);
    date->year = ((packed_date / 512) & 0x00ef) + 1980;
    date->month = ((packed_date & 0x01e0) / 32) & 0x000f;
    date->day = packed_date & 0x001f;
}


uint16_t smbus_manufacturer_read_word(uint16_t command) {
    uint16_t data = 0xaaaa;
    if (smbus_write_word(MANUFACTURER_ACCESS, command) < 0) {
        perror("Writ to MANUFACTURER_ACCESS failed");
        return 0;
    };
    if(smbus_read_word(MANUFACTURER_ACCESS, &data) < 0) {
        perror("MANUFACTURER_ACCESS (0x00)");
    };
    return data;
}

uint8_t is_sealed() {
    uint16_t opStatus = smbus_manufacturer_read_word(0x54);
    printf("OperationStatus     : 0x%04x  ", opStatus);
    if ((opStatus & 0b0100000000000000) == 0) printf("FAS ");
    if (opStatus & 0b0010000000000000) printf("SEALED ");
    if (opStatus & 0x0020) printf("DIS-FAULT ");
    if (opStatus & 0x0010) printf("DIS-DISABLED ");
    if (opStatus & 0x0008) printf("DIS-INHIBIT ");
    if ((opStatus & 0x0002) == 0) printf("!VOK ");
    if ((opStatus & 0x0001) == 0) printf("!QEN ");
    printf("\n");
    return (opStatus & 0b0010000000000000) != 0;
}

void unseal() {
    // Unseal battery - 36720414
    // @see https://e2e.ti.com/support/power-management-group/power-management/f/power-management-forum/1057725/bq27541-how-to-find-out-that-unseal-key-is-correct
    if(smbus_write_word(MANUFACTURER_ACCESS, htonl(0x0414))) {
        perror("Unseal w1");
    } else if(smbus_write_word(MANUFACTURER_ACCESS, htonl(0x3672))) {
        perror("Unseal w2");
    } else {
        puts("write unseal data succeeded\n");
    }
}

static void try_unseal() {
    uint8_t sealed = is_sealed();
    if (sealed) {
        printf("> Device is sealed; trying to unseal\n");
        unseal();
        if (is_sealed()) {
            printf(">>> Device is still sealed :(\n");
        } else {
            printf(">>> Device seems to be unsealed now :)\n");
        }
    }
}
