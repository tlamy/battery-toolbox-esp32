//
// Created by Thomas Lamy on 12.02.23.
//

#include <Arduino.h>
#include "smbus.h"

int smbus_fetch_word(unit8_t device, byte func) {
    i2c_start(device << 1 | I2C_WRITE);
    i2c_write(func);
    i2c_rep_start(device << 1 | I2C_READ);
    byte b1 = i2c_read(false);
    byte b2 = i2c_read(true);
    i2c_stop();
    return (int) b1 | (((int) b2) << 8);
}

uint8_t smbus_read_block(unit8_t device, uint8_t command, uint8_t *blockBuffer, uint8_t blockBufferLen) {
    uint8_t x, num_bytes;
    i2c_start((device << 1) + I2C_WRITE);
    i2c_write(command);
    i2c_rep_start((device << 1) + I2C_READ);
    num_bytes = i2c_read(false); // num of bytes; 1 byte will be index 0
    num_bytes = constrain(num_bytes, 0, blockBufferLen - 2); // room for null at the end
    for (x = 0; x < num_bytes - 1; x++) { // -1 because x=num_bytes-1 if x<y; last byte needs to be "nack"'d, x<y-1
        blockBuffer[x] = i2c_read(false);
    }
    blockBuffer[x++] = i2c_read(true); // this will nack the last byte and store it in x's num_bytes-1 address.
    blockBuffer[x] = 0; // and null it at last_byte+1
    i2c_stop();
    return num_bytes;
}

byte smbus_scan(byte* devices)
{
    byte num_found = 0;
    byte i = 0;
    for (i = 0; i < 127; i++) {
        bool ack = i2c_start(i << 1 | I2C_WRITE);
        if (ack) {
            devices[num_found] = i;
            num_found++;
        }
        i2c_stop();
    }
    return num_found;
}
