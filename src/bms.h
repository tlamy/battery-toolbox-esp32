//
// Created by Thomas Lamy on 12.02.23.
//

#ifndef BATTERY_TOOLBOX_ESP32_BMS_H
#define BATTERY_TOOLBOX_ESP32_BMS_H
#include "sys/types.h"

struct date {
    uint16_t year;
    uint8_t month;
    uint8_t day;
};

extern float bat_temp_c();

extern float bat_pack_volts();

extern float bat_cell_volts(unsigned char cell);

extern float bat_pack_amps();

extern float bat_pack_amps_average();

extern float bat_capacity_remaining();

extern float bat_capacity();

extern float bat_design_capacity();

extern float bat_design_volts();

extern uint16_t bat_status();

extern uint16_t bat_cycle_count();

extern unsigned char *bat_manufacturer(unsigned char *buffer);

extern unsigned char *bat_devicename(unsigned char *buffer);

extern unsigned char *bat_chemistry(unsigned char *buffer);

extern void bat_manufactured_date(struct date *date);

extern uint8_t is_sealed();

extern void unseal();

extern void try_unseal();

#endif //BATTERY_TOOLBOX_ESP32_BMS_H
