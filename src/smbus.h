//
// Created by Thomas Lamy on 12.02.23.
//

#ifndef BATTERY_TOOLBOX_ESP32_SMBUS_H
#define BATTERY_TOOLBOX_ESP32_SMBUS_H

extern int smbus_fetch_word(unit8_t device,byte func);

extern uint8_t smbus_read_block(unit8_t device,uint8_t command, uint8_t *blockBuffer, uint8_t blockBufferLen);

extern byte smbus_scan(byte *devices);

#endif //BATTERY_TOOLBOX_ESP32_SMBUS_H
