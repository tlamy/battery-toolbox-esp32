#include <Arduino.h>


void setup() {
  Serial.begin(9600);
  pinMode(18, OUTPUT);
}

void loop() {
  //Serial.println("Ping");
  digitalWrite(18, HIGH); // turn the LED on
  delay(100);             // wait for 500 milliseconds
  digitalWrite(18, LOW);  // turn the LED off
  delay(400);             // wait for 500 milliseconds
}